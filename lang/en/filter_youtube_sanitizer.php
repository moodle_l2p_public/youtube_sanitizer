<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

$string['filtername'] = 'YouTube Sanitizer';
$string['pluginname'] = 'YouTube Sanitizer';
$string['privacy:metadata'] = 'YouTube Sanitizer doesn\'t store any personal data.';
$string['cachedef_videoinfo'] = 'Stores information about videos, e.g. html embed code and thumbnail.';
$string['privacynote'] = 'To protect your privacy, the video will only be loaded after you click the play button. By playing the video, you consent to the transmission of your data to YouTube in accordance with {$a}.';
$string['privacyurltext'] = 'the Google privacy policy';
$string['privacyurl'] = 'https://policies.google.com/privacy?hl=en';
$string['oembederror'] = 'Error loading video embed information.';
$string['thumbnailerror'] = 'Error loading video thumbnail.';

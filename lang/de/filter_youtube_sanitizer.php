<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

$string['filtername'] = 'YouTube Sanitizer';
$string['pluginname'] = 'YouTube Sanitizer';
$string['privacy:metadata'] = 'Der YouTube Sanitizer speichert keine persönlichen Daten.';
$string['cachedef_videoinfo'] = 'Speichert Informationen über Videos, z.B. HTML-Einbettungscodes und Vorschaubilder.';
$string['privacynote'] = 'Um Ihre Privatsphäre zu schützen, wird das Video erst nach Klick auf den Play-Button geladen. Durch das Abspielen des Videos stimmen Sie der Übertragung Ihrer Daten an YouTube gemäß {$a} zu.';
$string['privacyurltext'] = 'der Google-Datenschutzerklärung';
$string['privacyurl'] = 'https://policies.google.com/privacy?hl=de';
$string['oembederror'] = 'Fehler beim Laden der Videoeinbettungsinformationen.';
$string['thumbnailerror'] = 'Fehler beim Laden des Vorschaubildes.';